import http from 'k6/http';
import { sleep, check } from 'k6';

export const options = {
    stages: [
        {duration: '5s', target: 40},
        {duration: '1m', target: 80},
        {duration: '30s', target: 0},
    ],
    thresholds: {
        'http_req_duration': ['p(99)<2000'], // 99% of requests must complete below 1.5s
        'logged in successfully': ['p(99)<2000'], // 99% of requests must complete below 1.5s
    }
}

const BASE_URL = 'http://localhost:8083';

export default function () {

    // const payload = JSON.stringify({
    //     "SupplierInvoice":"5105600735",
    //     "FiscalYear": "2021",
    //     "SuplrInvcTransactionCategory":"RD"
    // });

    const params = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const res = http.get(`${BASE_URL}/timeout/teste/cpu/200`,  params);
    sleep(1);

    const checkRes = check(res, {
        'status is 200': (r) => r.status === 200
    });
}
