package br.com.rogeriosouza.sprintbootgeral.redisson.topic;

import org.redisson.api.RPatternTopic;
import org.redisson.api.RReliableTopic;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ListenerTopic {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListenerTopic.class);

    public static final String TOPIC_1 = "topic:1";
    public static final String TOPIC_2 = "topic:2";
    public static final String TOPIC_1_2 = "topic:*";
    public static final String RELIABLE_TOPIC_1 = "topicreliable";

    @Autowired
    private RedissonClient redissonClient;

    @PostConstruct
    public void postConstruct() {

        LOGGER.info("Registrando: {}", TOPIC_1);
        this.redissonClient.getTopic(TOPIC_1).addListener(String.class, (channel, msg) -> {
            LOGGER.info("PubSub - Channel:[{}] - Message:[{}]", channel, msg);
        });

        LOGGER.info("Registrando: {}", TOPIC_2);
        this.redissonClient.getTopic(TOPIC_2).addListener(String.class, (channel, msg) -> {
            LOGGER.info("PubSub - Channel:[{}] - Message:[{}]", channel, msg);
        });

        LOGGER.info("Registrando multiples topics: {}", TOPIC_1_2);
        RPatternTopic patternTopic = redissonClient.getPatternTopic(TOPIC_1_2);
        patternTopic.addListener(String.class, (pattern, channel, msg) -> {
            LOGGER.info("PubSub - Pattern [{}] - Channel:[{}] - Message:[{}]", pattern, channel, msg);
        });

        LOGGER.info("Registrando reliable topics: {}", RELIABLE_TOPIC_1);
        RReliableTopic reliableTopic = redissonClient.getReliableTopic(RELIABLE_TOPIC_1);
        reliableTopic.addListener(String.class, (channel, msg) -> {
            LOGGER.info("PubSub - Channel:[{}] - Message:[{}]", channel, msg);
        });

    }
}
