package br.com.rogeriosouza.sprintbootgeral.gracefulshutdown;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ThreadSleepService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadSleepService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public void testeAsync(int delay, String id) {
        this.slowlyRequest(delay, id);

    }

    public void teste(int delay, String id) {
        this.slowlyRequest(delay, id);

    }

    private void slowlyRequest(int delay, String id) {
        LOGGER.info("{} Inicio Request", id);
        String forObject = this.restTemplate.getForObject("http://slowwly.robertomurray.co.uk/delay/" + delay + "/url/http://google.co.uk", String.class);
        LOGGER.info("{} Fim Request", id);
    }
}
