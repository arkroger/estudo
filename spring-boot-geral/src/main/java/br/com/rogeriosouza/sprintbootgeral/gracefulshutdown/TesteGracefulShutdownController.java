package br.com.rogeriosouza.sprintbootgeral.gracefulshutdown;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("gracefulshutdown")
public class TesteGracefulShutdownController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TesteGracefulShutdownController.class);

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private ThreadSleepService threadSleepService;

    @GetMapping
    public boolean teste() {

        threadSleepService.teste(20_000, "Controller Sync");

        return true;

    }

    @GetMapping("async")
    public boolean testeAsync() {

        threadSleepService.testeAsync(20_000, "Controller Async");

        return true;

    }
}
