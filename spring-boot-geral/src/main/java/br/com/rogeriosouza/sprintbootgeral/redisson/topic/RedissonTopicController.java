package br.com.rogeriosouza.sprintbootgeral.redisson.topic;

import org.redisson.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("redisson/topic")
public class RedissonTopicController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedissonTopicController.class);

    @Autowired
    private RedissonClient redissonClient;

    @PostMapping("1")
    public void publish() {
        RTopic topic = redissonClient.getTopic(ListenerTopic.TOPIC_1);
        topic.publish("Mensagem: " + LocalDateTime.now());

    }

    @PostMapping("2")
    public void publish2() {
        RTopic topic = redissonClient.getTopic(ListenerTopic.TOPIC_2);
        topic.publish("Mensagem: " + LocalDateTime.now());
    }

    @PostMapping("reliable")
    public void publishReliable() {
        RReliableTopic reliableTopic = redissonClient.getReliableTopic(ListenerTopic.RELIABLE_TOPIC_1);
        reliableTopic.publish("Mensagem: " + LocalDateTime.now());

    }



}
