package br.com.rogeriosouza.sprintbootgeral.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@EnableScheduling
@Component
@EnableAsync
public class SchedullerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchedullerTest.class);

    //@Scheduled(fixedRate = 1000)
    public void schedule() {
        LOGGER.info("INICIO Schedule1");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info("FIM Schedule1");

    }

    /*@Scheduled(fixedDelay = 1000)
    public void schedule2() {
        LOGGER.info("Schedule2");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info("FIM Schedule2");

    }
*/
    //@Scheduled(fixedDelay = 1000)
    public void schedule3() {
        LOGGER.info("Schedule3");

    }
}
