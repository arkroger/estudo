package br.com.rogeriosouza.sprintbootgeral.gracefulshutdown;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class ScheduleGracefulShutdown {

    @Autowired
    private ThreadSleepService threadSleepService;

    //@Scheduled(fixedDelay = 3000)
    public void teste() {
        threadSleepService.teste(20_000, "Schedule");
    }

}
