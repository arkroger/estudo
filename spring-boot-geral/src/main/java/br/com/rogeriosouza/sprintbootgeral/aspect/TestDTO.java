package br.com.rogeriosouza.sprintbootgeral.aspect;

import java.util.Random;

public class TestDTO {
    private int id;
    private String nome;

    public TestDTO() {
        this.id = new Random().nextInt(99999);
        this.nome = "Teste";
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return "TestDTO{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                '}';
    }
}
