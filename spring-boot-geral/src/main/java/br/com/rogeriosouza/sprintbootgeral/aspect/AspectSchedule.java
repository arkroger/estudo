package br.com.rogeriosouza.sprintbootgeral.aspect;


import org.aspectj.weaver.ast.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Random;

@EnableScheduling
@Component
public class AspectSchedule {

    @Autowired
    private AspectTest aspectTest;

    private static final Logger LOGGER = LoggerFactory.getLogger(AspectSchedule.class);

    //@Scheduled(fixedRate = 3000)
    public void schedule() throws Exception {
        LOGGER.info("======================================");
        LOGGER.info("INICIO Schedule");
        TestDTO testDTO = new TestDTO();
        LOGGER.info("Parametro {}", testDTO);
        aspectTest.testeAspect("arg String", testDTO);
        LOGGER.info("FIM Schedule");
        LOGGER.info("======================================");
    }


}
