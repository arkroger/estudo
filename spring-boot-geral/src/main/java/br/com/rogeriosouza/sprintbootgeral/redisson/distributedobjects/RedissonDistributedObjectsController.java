package br.com.rogeriosouza.sprintbootgeral.redisson.distributedobjects;

import br.com.rogeriosouza.sprintbootgeral.redisson.SomeObject;
import org.redisson.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("redisson/distributed-objects")
public class RedissonDistributedObjectsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedissonDistributedObjectsController.class);

    @Autowired
    private RedissonClient redissonClient;

    @GetMapping
    public void teste() {
        RAtomicLong atomicLong = redissonClient.getAtomicLong("atomicLong");
        long l = atomicLong.incrementAndGet();
        LOGGER.info("TESTE {}", l);
    }

    @GetMapping("bucket")
    public void bucket() {
        RBucket<Long> bucket = redissonClient.getBucket("bucket");
        bucket.set(5L);
        bucket.expire(20, TimeUnit.SECONDS);
    }

    @GetMapping("bitset")
    public void bitset() {
        RBitSet bitset = redissonClient.getBitSet("bitset");
        bitset.set(0, true);
        bitset.set(1, false);
        bitset.set(2, true);
    }

    @GetMapping("bloomFilter")
    public void bloomFilter() {
        RBloomFilter<SomeObject> bloomFilter = redissonClient.getBloomFilter("bloomFilter");
        bloomFilter.tryInit(55000000L, 0.03);
        bloomFilter.add(new SomeObject("valor1","valor2"));
        bloomFilter.add(new SomeObject("valor5","valor6"));
        bloomFilter.contains(new SomeObject("valor1", "valor6"));
        LOGGER.info("Count: {}", bloomFilter.count());
    }

    @GetMapping("rateLimiter")
    public void rateLimiter() {
        RRateLimiter rateLimiter = redissonClient.getRateLimiter("rateLimiter");
        rateLimiter.trySetRate(RateType.OVERALL, 5, 30, RateIntervalUnit.SECONDS);

        LOGGER.info("Acquire");
        rateLimiter.acquire();
        LOGGER.info("Acquired");

    }
}
