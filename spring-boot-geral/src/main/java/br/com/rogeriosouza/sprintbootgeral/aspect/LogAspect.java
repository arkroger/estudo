package br.com.rogeriosouza.sprintbootgeral.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.weaver.ast.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogAspect.class);

    @Before("@annotation(LogExecutionTime)")
    public void before(JoinPoint joinPoint) throws Exception {

        TestDTO argument = null;
        for (Object arg : joinPoint.getArgs()) {
            if (arg instanceof TestDTO) {
                argument = (TestDTO) arg;
            }
        }

        LOGGER.info("BEFORE LogExecutionTime {}", argument);
        //throw new Exception("OPS");
    }

    @AfterReturning(pointcut = "@annotation(LogExecutionTime)", returning = "returnValue")
    public void afterReturn(JoinPoint joinPoint, TestDTO returnValue) {

        LOGGER.info("AFTER_RETURN LogExecutionTime {}", returnValue);
    }

    @AfterThrowing(pointcut = "@annotation(LogExecutionTime)", throwing = "ex")
    public void afterThrowing(JoinPoint joinPoint, Exception ex) {
        LOGGER.info("AFTER_THROWING LogExecutionTime");
    }

    @After("@annotation(LogExecutionTime)")
    public void after(JoinPoint joinPoint) {
        LOGGER.info("AFTER LogExecutionTime");
    }

    @Around("@annotation(LogExecutionTime)")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();


        Object proceed = joinPoint.proceed();
        long executionTime = System.currentTimeMillis() - start;

        LOGGER.info("{} executed in {}ms", joinPoint.getSignature(),executionTime );

        return proceed;
    }
}
