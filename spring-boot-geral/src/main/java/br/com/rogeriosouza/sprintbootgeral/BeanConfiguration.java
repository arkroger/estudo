package br.com.rogeriosouza.sprintbootgeral;

import org.springframework.boot.task.TaskSchedulerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.HashSet;

@EnableScheduling
@EnableAsync
@Configuration
public class BeanConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


    @Bean
    public TaskScheduler taskScheduler() {
        return new TaskSchedulerBuilder()
                .poolSize(5)
                .awaitTermination(true)
                .awaitTerminationPeriod(Duration.ofSeconds(30))
                .build();
    }
}
