package br.com.rogeriosouza.sprintbootgeral.redisson.distributedcollections;

import br.com.rogeriosouza.sprintbootgeral.redisson.SomeObject;
import org.redisson.api.*;
import org.redisson.api.map.event.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("redisson/distributed-collections")
public class DistributedCollectionsController {

    private static final String MAP_CACHE = "mapcache";

    private static final Logger LOGGER = LoggerFactory.getLogger(DistributedCollectionsController.class);
    @Autowired
    private RedissonClient redissonClient;

    private RMapCache<String, SomeObject> map;

    @PostConstruct
    public void postConstruct() {
        map = redissonClient.getMapCache(MAP_CACHE);

        map.addListener((EntryCreatedListener<String, SomeObject>) event -> {
            LOGGER.info("Mapa criado: {}", event.getKey());
        });

        map.addListener((EntryExpiredListener<String, SomeObject>) event -> {
            LOGGER.info("Mapa expirado: {}", event.getKey());
        });

        map.addListener((EntryUpdatedListener<String, SomeObject>) event -> {
            LOGGER.info("Mapa atualizado: {}", event.getKey());
        });

        map.addListener((EntryRemovedListener<String, SomeObject>) event -> {
            LOGGER.info("Mapa removido: {}", event.getKey());
        });
    }

    @GetMapping("map")
    public void map() {
        RMap<String, SomeObject> map = redissonClient.getMap("map");
        map.put("1", new SomeObject("1", "2"));
        map.put("2", new SomeObject("1", "2"));


        SomeObject someObject = map.get("1");
        LOGGER.info("Map: {}", someObject);
    }

    @GetMapping("mapCache")
    public void mapCache() {

        map.put("1", new SomeObject("1", "2"), 30, TimeUnit.SECONDS);
        map.put("2", new SomeObject("3", "4"), 10, TimeUnit.SECONDS);

        SomeObject someObject = map.get("1");
        LOGGER.info("Map: {}", someObject);
    }

    @PostMapping("mapCache")
    public void atualizarMapCache(@RequestBody SomeObject someObject) {
        map.put(someObject.getValor1(), someObject, 10, TimeUnit.SECONDS);
    }

    @DeleteMapping("mapCache")
    public void apagarMapCache(@RequestBody SomeObject someObject) {
        map.remove(someObject.getValor1());
    }

    @GetMapping("set")
    public void set() {
        RSet<SomeObject> set = redissonClient.getSet("set");
        set.add(new SomeObject("1", "2"));
        set.add(new SomeObject("3", "4"));
        set.parallelStream().forEach(someObject -> LOGGER.info("Objecto: {}", someObject));
    }

    @GetMapping("list")
    public void list() {
        RList<Object> list = redissonClient.getList("list");
        list.add(new SomeObject("1", "2"));
        list.add(new SomeObject("3", "4"));
        list.parallelStream().forEach(someObject -> LOGGER.info("Objecto: {}", someObject));
    }
}
