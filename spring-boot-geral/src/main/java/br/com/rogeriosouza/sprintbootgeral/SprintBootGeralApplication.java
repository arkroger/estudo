package br.com.rogeriosouza.sprintbootgeral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@SpringBootApplication
public class SprintBootGeralApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprintBootGeralApplication.class, args);
	}

}
