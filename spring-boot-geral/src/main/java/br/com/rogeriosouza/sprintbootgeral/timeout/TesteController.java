package br.com.rogeriosouza.sprintbootgeral.timeout;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/timeout/teste")
public class TesteController {

    private static Logger LOGGER = LoggerFactory.getLogger(TesteController.class);


    private AtomicLong atomicLong = new AtomicLong();



    @GetMapping("/{time}")
    public boolean teste(@PathVariable("time") String time) throws Exception {

        try {
            long value = atomicLong.getAndIncrement();
            LOGGER.info("init [" + value + "]");
            Thread.sleep(Long.parseLong(time));
            LOGGER.info("finish [" + value + "]");
        } catch (Exception e) {
            e.printStackTrace();
        }
        

        return true;
    }


}
