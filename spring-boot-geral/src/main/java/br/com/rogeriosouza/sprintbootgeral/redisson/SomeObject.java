package br.com.rogeriosouza.sprintbootgeral.redisson;

import java.io.Serializable;

public class SomeObject implements Serializable {

    private static final long serialVersionUID = -1334492784157599547L;
    private String valor1;
    private String valor2;

    public SomeObject(String valor1, String valor2) {
        this.valor1 = valor1;
        this.valor2 = valor2;
    }

    public String getValor1() {
        return valor1;
    }

    public String getValor2() {
        return valor2;
    }

    @Override
    public String toString() {
        return "SomeObject{" +
                "valor1='" + valor1 + '\'' +
                ", valor2='" + valor2 + '\'' +
                '}';
    }
}
