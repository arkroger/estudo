package br.com.rogeriosouza.estudo.springbootmetrics.gauge.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
public class GaugeService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GaugeService.class);
    public static String PDF_COM_ERRO = "pdf.erro";
    public static String PDF_EM_PROCESSAMENTO_COM_ERRO_CERTIFICACAO = "pdf.emProcesamentoComErroCertificacao";
    public static String PDF_JA_PROCESSADO_COM_ERRO_CERTIFICACAO = "pdf.jaProcessadoComErroCertificacao";
    public static String PDF_ERRO_GERACAO_DOCUMENTO_PARA_CERTIFICAR = "pdf.erroGeracaoDocumentoParaCertificar";
    public static String PDF_CERTIFICANDO = "pdf.certificando";
    public static String PDF_PENDENTE_CERTIFICACAO = "pdf.pendenteCertificacao";

    private final static Map<String, Integer> mapGauge = new HashMap<>();

    public GaugeService() {
        mapGauge.put(PDF_COM_ERRO, 0);
        mapGauge.put(PDF_EM_PROCESSAMENTO_COM_ERRO_CERTIFICACAO, 0);
        mapGauge.put(PDF_JA_PROCESSADO_COM_ERRO_CERTIFICACAO, 0);
        mapGauge.put(PDF_ERRO_GERACAO_DOCUMENTO_PARA_CERTIFICAR, 0);
        mapGauge.put(PDF_CERTIFICANDO, 0);
        mapGauge.put(PDF_PENDENTE_CERTIFICACAO, 0);
    }

    @PostConstruct
    public void postConstruct() {
        this.atualizarGaugePrioritario();
        this. atualizarGaugePrioridadeMedia();
        this. atualizarGaugeNaoPrioritario();
    }

    public void atualizarGaugePrioritario() {
        LOGGER.info("Atualizando Gauge Prioritario");
        mapGauge.put(GaugeService.PDF_PENDENTE_CERTIFICACAO, new Random().nextInt(1000));
        mapGauge.put(GaugeService.PDF_COM_ERRO, new Random().nextInt(600));
        mapGauge.put(GaugeService.PDF_EM_PROCESSAMENTO_COM_ERRO_CERTIFICACAO, new Random().nextInt(9999));
    }

    public void atualizarGaugePrioridadeMedia() {
        LOGGER.info("Atualizando Gauge Prioridade média");
        mapGauge.put(GaugeService.PDF_JA_PROCESSADO_COM_ERRO_CERTIFICACAO, new Random().nextInt(9999));
        mapGauge.put(GaugeService.PDF_ERRO_GERACAO_DOCUMENTO_PARA_CERTIFICAR, new Random().nextInt(600));
    }

    public void atualizarGaugeNaoPrioritario() {
        LOGGER.info("Atualizando Gauge não prioritário");
        mapGauge.put(GaugeService.PDF_CERTIFICANDO, new Random().nextInt(600));
    }

    public int quantidadeErroGeracaoDocumentoParaCertificar() {
        return mapGauge.get(PDF_ERRO_GERACAO_DOCUMENTO_PARA_CERTIFICAR);
    }

    public int quantidadeCertificando() {
        return mapGauge.get(PDF_CERTIFICANDO);
    }

    public int quantidadeErroProcessando() {
        return mapGauge.get(PDF_EM_PROCESSAMENTO_COM_ERRO_CERTIFICACAO);
    }

    public int quantidadeErroJaProcessado() {
        return mapGauge.get(PDF_JA_PROCESSADO_COM_ERRO_CERTIFICACAO);
    }

    public int quantidadePendenteProcessamento() {
        return mapGauge.get(PDF_PENDENTE_CERTIFICACAO);
    }

    public int quantidadePdfErro() {
        return mapGauge.get(PDF_COM_ERRO);
    }
}
