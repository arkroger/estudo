package br.com.rogeriosouza.estudo.springbootmetrics.rest;

import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/timed")
public class TimedRest {

    private static Logger LOGGER = LoggerFactory.getLogger(TimedRest.class);

    @Timed
    @GetMapping("")
    public boolean counter() {

        try {
            float time = new Random().nextInt(8);
            int sleep = (int) ((time/2)*1000);
            LOGGER.info("TIMED - Sleep: {}", sleep);
            Thread.sleep(sleep);
        } catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }


}
