package br.com.rogeriosouza.estudo.springbootmetrics;

import br.com.rogeriosouza.estudo.springbootmetrics.gauge.service.GaugeService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@EnableScheduling
@SpringBootApplication
public class SpringBootMetricsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMetricsApplication.class, args);
	}

	@Bean()
	public Counter counterSuccess(MeterRegistry registry) {
		return Counter.builder("xpto2.events.sucesso").register(registry);
	}

	@Bean()
	public Counter counterError(MeterRegistry registry) {
		return Counter.builder("xpto2.events.erro").register(registry);
	}

	@Bean
	public Gauge gaugeErroAguardandoProcessamento(MeterRegistry registry, GaugeService gaugeService) {
		return Gauge.builder(GaugeService.PDF_EM_PROCESSAMENTO_COM_ERRO_CERTIFICACAO, gaugeService::quantidadeErroProcessando).register(registry);
	}

	@Bean
	public Gauge gaugeErroJaProcessado(MeterRegistry registry, GaugeService gaugeService) {
		return Gauge.builder(GaugeService.PDF_JA_PROCESSADO_COM_ERRO_CERTIFICACAO, gaugeService::quantidadeErroJaProcessado).register(registry);
	}

	@Bean
	public Gauge gaugePendenteAguardandoProcessamento(MeterRegistry registry, GaugeService gaugeService) {
		return Gauge.builder(GaugeService.PDF_PENDENTE_CERTIFICACAO, gaugeService::quantidadePendenteProcessamento).register(registry);
	}

	@Bean
	public Gauge gaugePdfComErro(MeterRegistry registry, GaugeService gaugeService) {
		return Gauge.builder(GaugeService.PDF_COM_ERRO, gaugeService::quantidadePdfErro).register(registry);
	}

	@Bean
	public Gauge gaugePdfCertificando(MeterRegistry registry, GaugeService gaugeService) {
		return Gauge.builder(GaugeService.PDF_CERTIFICANDO, gaugeService::quantidadeCertificando).register(registry);
	}

	@Bean
	public Gauge gaugePdfErroGEracaoDocumentoParaCertificar(MeterRegistry registry, GaugeService gaugeService) {
		return Gauge.builder(GaugeService.PDF_ERRO_GERACAO_DOCUMENTO_PARA_CERTIFICAR, gaugeService::quantidadeErroGeracaoDocumentoParaCertificar).register(registry);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
