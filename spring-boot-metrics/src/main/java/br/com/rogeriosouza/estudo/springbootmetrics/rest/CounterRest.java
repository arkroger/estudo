package br.com.rogeriosouza.estudo.springbootmetrics.rest;

import io.micrometer.core.instrument.Counter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/counter")
public class CounterRest {

    private static Logger LOGGER = LoggerFactory.getLogger(CounterRest.class);


    @Autowired
    private Counter counterSuccess;

    @Autowired
    private Counter counterError;


    @GetMapping("")
    public boolean counter() throws Exception {

        boolean sucess = new Random().nextBoolean();
        if (sucess) {
            counterSuccess.increment();
            LOGGER.info("Count: {}", counterSuccess.count());
        } else {
            counterError.increment();
            LOGGER.error("Count: {}", counterError.count());
        }



        return true;
    }


}
