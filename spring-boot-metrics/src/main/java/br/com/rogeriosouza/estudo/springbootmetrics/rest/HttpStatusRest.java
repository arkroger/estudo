package br.com.rogeriosouza.estudo.springbootmetrics.rest;

import io.micrometer.core.annotation.Timed;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


@RestController
@RequestMapping("/httpStatus")
public class HttpStatusRest {

    @Timed
    @GetMapping()
    public ResponseEntity<String> random() {
        List<HttpStatus> listHttpStatus = Arrays.asList(
                HttpStatus.OK,
                HttpStatus.OK,
                HttpStatus.INTERNAL_SERVER_ERROR,
                HttpStatus.NOT_FOUND,
                HttpStatus.UNAUTHORIZED
        );
        int random = new Random().nextInt(listHttpStatus.size()-1);
        HttpStatus httpStatus = listHttpStatus.get(random);
        return new ResponseEntity<>(null, httpStatus);
    }
}
