package br.com.rogeriosouza.estudo.springbootmetrics.gauge.schedule;

import br.com.rogeriosouza.estudo.springbootmetrics.gauge.service.GaugeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@EnableScheduling
@Component
public class GaugeSchedule {

    @Autowired
    private GaugeService gaugeService;

    @Scheduled(cron = "*/30 * * * * *")//hora em hora
    public void atualizarGauge() {
        gaugeService.atualizarGaugePrioritario();
    }


    @Scheduled(cron = "0 * * * * *")//+- 2x por dia
    public void atualizarGaugePrioridadeMedia() {
        gaugeService.atualizarGaugePrioridadeMedia();


    }

    @Scheduled(cron = "0 */2 * * * *")//+- max 4x por dia
    public void atualizarGaugeNaoPrioritario() {
        gaugeService.atualizarGaugeNaoPrioritario();
    }
}
