# Grafana
* O docker-composer irá criar o diretorio `/data/grafana6.5.3` para o volume do grafana.
* `sudo chown -R 472:472 /data/grafana6.5.3 `
* O Grafana irá subir na porta 3000.

# Prometheus
* O Promethues irá subir na porta 9090.

# Execução
* O Grafana e o Prometheus serão executados com network_mode Host, portando as portas 3000 e 9090 devem estar livres.
* No diretório do docker-compose.yml executar:<br> `docker-compose up`
* Rodar aplicação springboot na porta 8080.
* Acessar o grafana localhost:3000. No primeiro acesso será solicitado usuário (admin) e senha (admin):<br>
* No grafana, realizar a importação do dashboard `JVM(Micrometer)-customizado.json`

# Build da imagem no minikube
`eval $(minikube docker-env)`<br>
`docker build --build-arg JAR_FILE=target/*.jar -t testeapp`

