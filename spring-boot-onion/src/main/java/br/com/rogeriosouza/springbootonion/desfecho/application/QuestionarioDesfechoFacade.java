package br.com.rogeriosouza.springbootonion.desfecho.application;


import br.com.rogeriosouza.springbootonion.desfecho.domain.model.domainValue.NovoQuestionarioDesfecho;

public interface QuestionarioDesfechoFacade {
    void preencher(NovoQuestionarioDesfecho novoQuestionarioDesfecho);
}
