package br.com.rogeriosouza.springbootonion.desfecho.application;

import br.com.rogeriosouza.springbootonion.desfecho.domain.model.Beneficiario;
import br.com.rogeriosouza.springbootonion.desfecho.domain.model.Medico;
import br.com.rogeriosouza.springbootonion.desfecho.domain.model.Pergunta;
import br.com.rogeriosouza.springbootonion.desfecho.domain.model.QuestionarioDesfecho;
import br.com.rogeriosouza.springbootonion.desfecho.domain.model.domainValue.NovoQuestionarioDesfecho;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionarioDesfechoService implements QuestionarioDesfechoFacade {

    @Override
    public void preencher(NovoQuestionarioDesfecho novoQuestionarioDesfecho) {
        Medico medico = novoQuestionarioDesfecho.getMedico();
        List<Pergunta> perguntas = novoQuestionarioDesfecho.getPerguntas();
        Beneficiario beneficiario = novoQuestionarioDesfecho.getBeneficiario();

        //TODO REALIZAR VALIDAÇÕES

        QuestionarioDesfecho questionarioDesfecho = new QuestionarioDesfecho();
        medico.preencheNovo(questionarioDesfecho.comAsPerguntas(perguntas)).paraO(beneficiario);

        System.out.println(questionarioDesfecho);
    }
}
