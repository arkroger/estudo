package br.com.rogeriosouza.springbootonion.desfecho.domain.model.domainValue;

import br.com.rogeriosouza.springbootonion.desfecho.domain.model.Beneficiario;
import br.com.rogeriosouza.springbootonion.desfecho.domain.model.Medico;
import br.com.rogeriosouza.springbootonion.desfecho.domain.model.Pergunta;

import java.util.List;


public class NovoQuestionarioDesfecho {

    private Medico medico;
    private List<Pergunta> perguntas;
    private Beneficiario beneficiario;

    public NovoQuestionarioDesfecho(Medico medico, List<Pergunta> perguntas, Beneficiario beneficiario) {
        this.medico = medico;
        this.perguntas = perguntas;
        this.beneficiario = beneficiario;
    }

    public Medico getMedico() {
        return medico;
    }

    public List<Pergunta> getPerguntas() {
        return perguntas;
    }

    public Beneficiario getBeneficiario() {
        return beneficiario;
    }
}
