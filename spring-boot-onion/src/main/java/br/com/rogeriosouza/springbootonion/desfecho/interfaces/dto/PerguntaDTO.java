package br.com.rogeriosouza.springbootonion.desfecho.interfaces.dto;

import lombok.Data;

@Data
public class PerguntaDTO {
    private String pergunta;
    private String resposta;

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }
}
