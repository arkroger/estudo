package br.com.rogeriosouza.springbootonion.desfecho.interfaces;

import br.com.rogeriosouza.springbootonion.desfecho.application.QuestionarioDesfechoFacade;
import br.com.rogeriosouza.springbootonion.desfecho.domain.model.domainValue.NovoQuestionarioDesfecho;
import br.com.rogeriosouza.springbootonion.desfecho.interfaces.dto.RespostaQuestionarioDTO;
import br.com.rogeriosouza.springbootonion.desfecho.interfaces.mapper.RespostaQuestionarioDesfechoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/questionarioDesfecho")
public class QuestionarioDesfechoController {

    @Autowired
    private QuestionarioDesfechoFacade questionarioDesfechoFacade;

    @PostMapping
    public void novoQuestionarioDesfecho(@RequestBody RespostaQuestionarioDTO respostaQuestionarioDTO) {
        System.out.println("QUESTIONARIO DESFECHO");

        NovoQuestionarioDesfecho novoQuestionarioDesfecho = RespostaQuestionarioDesfechoMapper.toNovoQuestionarioDesfecho(respostaQuestionarioDTO);
        questionarioDesfechoFacade.preencher(novoQuestionarioDesfecho);
    }
}
