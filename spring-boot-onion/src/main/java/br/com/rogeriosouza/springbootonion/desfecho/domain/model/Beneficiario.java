package br.com.rogeriosouza.springbootonion.desfecho.domain.model;

import lombok.Data;
import lombok.Getter;


public class Beneficiario {
    String matricula;

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
