package br.com.rogeriosouza.springbootonion.desfecho.domain.model;

import org.springframework.beans.factory.annotation.Autowired;

public class Medico {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Autowired
    private IQuestionarioDesfechoRepository questionarioDesfechoRepository;

    public ParaBeneficiario preencheNovo(QuestionarioDesfecho questionarioDesfecho) {
        return beneficiario -> questionarioDesfechoRepository
                .criarNovo(
                        questionarioDesfecho
                                .preenchidoPor(this)
                                .paraO(beneficiario));

    }

    @FunctionalInterface
    public interface ParaBeneficiario {
        QuestionarioDesfecho paraO(Beneficiario beneficiario);
    }
}
