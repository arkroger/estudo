package br.com.rogeriosouza.springbootonion.desfecho.domain.model;

import lombok.Data;


public class Pergunta {
    private String descricao;
    private Resposta resposta;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Resposta getResposta() {
        return resposta;
    }

    public void setResposta(Resposta resposta) {
        this.resposta = resposta;
    }
}
