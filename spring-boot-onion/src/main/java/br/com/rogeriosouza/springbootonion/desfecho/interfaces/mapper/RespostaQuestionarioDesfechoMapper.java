package br.com.rogeriosouza.springbootonion.desfecho.interfaces.mapper;

import br.com.rogeriosouza.springbootonion.desfecho.domain.model.*;
import br.com.rogeriosouza.springbootonion.desfecho.domain.model.domainValue.NovoQuestionarioDesfecho;
import br.com.rogeriosouza.springbootonion.desfecho.interfaces.dto.RespostaQuestionarioDTO;

import java.util.ArrayList;
import java.util.List;

public class RespostaQuestionarioDesfechoMapper {

    public static NovoQuestionarioDesfecho toNovoQuestionarioDesfecho(RespostaQuestionarioDTO respostaQuestionarioDTO) {
        Medico medico = new Medico();
        medico.setId(respostaQuestionarioDTO.getMedico());

        Beneficiario beneficiario = new Beneficiario();
        beneficiario.setMatricula(respostaQuestionarioDTO.getMatricula());

        List<Pergunta> perguntas = new ArrayList<>();
        respostaQuestionarioDTO.getPerguntas().forEach(perguntaDTO -> {

            Resposta resposta = new Resposta();
            resposta.setResposta(perguntaDTO.getResposta());

            Pergunta pergunta = new Pergunta();
            pergunta.setDescricao(perguntaDTO.getPergunta());
            pergunta.setResposta(resposta);

            perguntas.add(pergunta);
        });

        return new NovoQuestionarioDesfecho(medico, perguntas, beneficiario);
    }
}
