package br.com.rogeriosouza.springbootonion.desfecho.domain.model;

import java.util.Date;
import java.util.List;

public class QuestionarioDesfecho {

    private Date dataPreenchimento;
    private Medico medico;
    List<Pergunta> perguntas;
    private Beneficiario beneficiario;

    public QuestionarioDesfecho comAsPerguntas(List<Pergunta> perguntas) {
        this.perguntas = perguntas;
        return this;
    }

    public QuestionarioDesfecho preenchidoPor(Medico medico) {
        this.dataPreenchimento = new Date();
        this.medico = medico;
        return this;
    }

    public QuestionarioDesfecho paraO(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
        return this;
    }

}
