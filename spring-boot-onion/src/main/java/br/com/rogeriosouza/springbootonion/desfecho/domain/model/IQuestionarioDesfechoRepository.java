package br.com.rogeriosouza.springbootonion.desfecho.domain.model;

public interface IQuestionarioDesfechoRepository {

    QuestionarioDesfecho criarNovo(QuestionarioDesfecho questionarioDesfecho);
}
