package br.com.rogeriosouza.springbootonion.desfecho.infrastructure.repository;

import br.com.rogeriosouza.springbootonion.desfecho.domain.model.IQuestionarioDesfechoRepository;
import br.com.rogeriosouza.springbootonion.desfecho.domain.model.QuestionarioDesfecho;
import org.springframework.stereotype.Component;

@Component
public class QuestionarioDesfechoRepository implements IQuestionarioDesfechoRepository {

    @Override
    public QuestionarioDesfecho criarNovo(QuestionarioDesfecho questionarioDesfecho) {
        System.out.println("CRIAR NOVO QUESTIONARIO DESFECHO");
        return questionarioDesfecho;
    }
}
