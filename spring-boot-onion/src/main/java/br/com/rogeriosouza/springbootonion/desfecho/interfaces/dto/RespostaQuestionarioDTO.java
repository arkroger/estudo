package br.com.rogeriosouza.springbootonion.desfecho.interfaces.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


public class RespostaQuestionarioDTO {
    private Long medico;
    private String matricula;
    private List<PerguntaDTO> perguntas;

    public Long getMedico() {
        return medico;
    }

    public void setMedico(Long medico) {
        this.medico = medico;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public List<PerguntaDTO> getPerguntas() {
        return perguntas;
    }

    public void setPerguntas(List<PerguntaDTO> perguntas) {
        this.perguntas = perguntas;
    }
}
