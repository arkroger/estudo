package br.com.rogeriosouza.springbootonion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootOnionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootOnionApplication.class, args);
	}

}
