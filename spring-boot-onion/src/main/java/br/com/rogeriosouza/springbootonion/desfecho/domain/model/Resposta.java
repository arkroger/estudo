package br.com.rogeriosouza.springbootonion.desfecho.domain.model;

import lombok.Data;


public class Resposta {
    private String resposta;

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }
}
