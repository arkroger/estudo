package br.com.rogeriosouza.controller;

import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@RegisterRestClient(configKey="appB-api")
@RegisterProvider(Filter.class)
@Path("/")
public interface AppBClient {
    @GET
    @Path("/hello")
    String getString();
}
