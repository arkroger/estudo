package br.com.rogeriosouza.controller;

import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingController {

    @Inject
    @RestClient
    private AppBClient appBClient;

    @GET
    @Retry(maxRetries = 4)
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
htop        String string = appBClient.getString();
        System.out.println(string);

        return "OK" + string;
    }
}
