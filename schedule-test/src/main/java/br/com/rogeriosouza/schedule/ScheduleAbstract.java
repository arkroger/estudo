package br.com.rogeriosouza.schedule;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Random;

public abstract class ScheduleAbstract {

    private static Logger LOGGER = LoggerFactory.getLogger(ScheduleAbstract.class);

    //protected static String LOCALHOST = "http://localhost:8080/";

    @Inject
    @Client("http://localhost:8080")
    private HttpClient httpClientSpring;

    @Inject
    @Client("http://localhost:8081")
    private HttpClient httpClientMicronaut;

    protected boolean random() {
        return new Random().nextBoolean();
    }

    protected void get(String url) {
        this.run(httpClientSpring, url);
        this.run(httpClientMicronaut, url);

        LOGGER.info("{} executado", url);
    }


    private void run(HttpClient httpClient, String url) {
        try {
            httpClient.toBlocking().retrieve(url);

        /*} catch (HttpServerErrorException e) {
            LOGGER.error("Status: {}", e.getStatusCode());*/
        } catch (HttpClientResponseException e) {
            LOGGER.error("ANIMAL: {} {}", e.getMessage(), e.getResponse().body());
        } catch (Exception e) {
            LOGGER.error("OPS: {}", e.getMessage());
        }
    }
}
