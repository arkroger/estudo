package br.com.rogeriosouza.schedule;

import io.micronaut.scheduling.annotation.Scheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

@Singleton
public class ScheduleTest extends ScheduleAbstract{

    private static Logger LOGGER = LoggerFactory.getLogger(ScheduleTest.class);


    @Scheduled(fixedDelay = "1s")
    public void schedule() {
        LOGGER.info("Iniciando schedule");

        if (random()) {
            this.testCounter();
        } else {
            LOGGER.info("Counter não executado");
        }

        if (random()) {
            this.testHttpStatus();
        } else {
            LOGGER.info("HttpStatus não executado");
        }
    }

    @Scheduled(fixedDelay = "1s")
    public void timed() {

        LOGGER.info("Iniciando schedule");

        if (random()) {
            this.testTimed();
        } else {
            LOGGER.info("Timed não executado");
        }
    }

    private void testTimed() {
        this.get("/timed");
    }

    private void testCounter() {
        this.get("/counter");
    }

    private void testHttpStatus() {
        this.get("/httpStatus");
    }


}
