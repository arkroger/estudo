package br.com.rogeriosouza.controller;

import io.micrometer.core.annotation.Timed;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

@Controller("/timed")
public class TimedController {

    private static Logger LOGGER = LoggerFactory.getLogger(TimedController.class);

    @Timed
    @Get
    public boolean counter() {
        try {
            float time = new Random().nextInt(8);
            int sleep = (int) ((time/2)*1000);
            LOGGER.info("TIMED - Sleep: {}", sleep);
            Thread.sleep(sleep);
        } catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}
