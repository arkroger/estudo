package br.com.rogeriosouza.controller;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

@Controller("/counter")
public class CounterController {

    private MeterRegistry meterRegistry;

    private static Logger LOGGER = LoggerFactory.getLogger(CounterController.class);

    public CounterController(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Get
    public boolean counter() {

        boolean sucess = new Random().nextBoolean();
        if (sucess) {
            Counter counter = meterRegistry.counter("xpto2.events.sucesso");
            counter.increment();
            LOGGER.info("Count: {}", counter.count());
        } else {
            Counter counter = meterRegistry.counter("xpto2.events.erro");
            counter.increment();
            LOGGER.error("Count: {}", counter.count());
        }

        return true;
    }
}
