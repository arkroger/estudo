package br.com.rogeriosouza.controller;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Controller;


import io.micrometer.core.annotation.Timed;
import io.micronaut.http.annotation.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Controller("/httpStatus")
public class HttpStatusController {

    private static Logger LOGGER = LoggerFactory.getLogger(TimedController.class);

    @Timed
    @Get
    public HttpStatus random() {
        List<HttpStatus> listHttpStatus = Arrays.asList(
                HttpStatus.OK,
                HttpStatus.OK,
                HttpStatus.INTERNAL_SERVER_ERROR,
                HttpStatus.BAD_REQUEST,
                HttpStatus.UNAUTHORIZED
        );
        int random = new Random().nextInt(listHttpStatus.size());
        HttpStatus httpStatus = listHttpStatus.get(random);
        LOGGER.info("HTTPSTATUS: {}", httpStatus.getCode());
        return httpStatus;
    }
}
